##### Description #####

The tool was created to compare input gene clusters to the closest relatives based on user input. It is possible
to provide custom genomes to search against. As search parameters one can either use a .hmm or protein .fasta file.

##### Download #####

Make sure you have installed and updated:
        - git (check: git --version)
        - anaconda (check: conda --version)

1. Open a terminal and got to the directory where the tool should be downloaded to
2. Enter: git clone https://bitbucket.org/jstahlecker/syn-view.git
3. Go to the new directory: cd syn-view
4. Create the conda envirnoment:
    - Linux: conda env create --file environment_linux.yml
    - Mac: conda env create --file environment_mac.yml
    - If you are having issues installing you can try: conda env create --file environment_basic.yml


##### Usage #####

## syn-view uses python3##
ACTIVATE the conda environment: conda activate SYN-view
To close the environment simply: conda deactivate

The following inputs must be provided

-i -> input genome(.gbff or .gbk) (annotated)
-search_mode -> either hmm or protein
-file -> either a hmm or fasta file

Choose:
        -mash_distances -> mash_distances.txt file from an autoMLST job (recommended)
        -custom_genomes -> path to the custom genomes. Keep in mind that the genomes must be assembled and annotated

Optional:
-automlst_parselength -> default 10. Specify how many genomes should be downloaded

Type python /your/path/to/syn-view/SYN-view.py --help for more options
Example inputs can be found in the example folder of syn-view:

You have a genome named GCF_000497425.1_S.niveus_v1_genomic.gbff and want to find hits based on a hmm called TIGR01059.HMM:

1. Run autoMLST (https://automlst.ziemertlab.com/) and download the mash_distances.txt file (also in the example folder)
2. put the file in the same folder as the gbff and the hmm files
3. Open the terminal and change to the directory (here it is example directory):
 cd /your/path/to/syn-view/example/
4. Run the python script:
        python /your/path/to/syn-view/SYN-view.py -i GCF_000497425.1_S.niveus_v1_genomic.gbff -search_mode hmm -file TIGR01059.HMM -mash_distances mash_distances.txt

You have a genome named GCF_000497425.1_S.niveus_v1_genomic.gbff and want to find hits based on a protein fasta file called gyrB_seq.fasta. You have already downloaded all sequences and moved them to a folder called custom_genomes:

1. put all files and folder in the same directory
2. From the same directory:
        python /your/path/to/syn-view/SYN-view.py -i GCF_000497425.1_S.niveus_v1_genomic.gbff -search_mode protein -file gyrB_seq.fasta -custom_genomes custom_genomes

You can always provide an absolute path for the input files

If you find this tool to be helpful, please cite:
Stahlecker, J.; Mingyar, E.; Ziemert, N.; Mungan, M.D. SYN-View: A Phylogeny-Based Synteny Exploration Tool for the Identification of Gene Clusters Linked to Antibiotic Resistance. Molecules 2021, 26, 144. https://doi.org/10.3390/molecules26010144 

